# PiRadio

This is a small web application that supports my homemade
kitchenradio based on raspberry pi.

## Usage

Install this package on the Pi and access it from any browser
via pi_address:3000

## Developing

Used frameworks are:
node.js
express
bootstrap
jade