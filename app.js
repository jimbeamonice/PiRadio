/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , stylus = require('stylus')
  , nib = require('nib')
  , url = require('url')
  , exec = require("child_process").exec
  , path = require('path');

var app = express();
//function compile(str, path) {
//	  return stylus(str)
//	    .set('filename', path)
//	    .use(nib())
//	}

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
//app.use(stylus.middleware(
//		  { src: __dirname + '/public'
//		  , compile: compile
//		  }));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

var stations = [ "empty" ];
var channels = [ { name: "empty" } ];
var currentChannel = 0;

app.get('/',
  function (req, res)
  {
    res.render('index',
	       { title : 'Home',
                 channels: channels,
                 channel: currentChannel }
  )}
);

app.get('/stop',
  function (req, res)
  {
    exec("mpc stop");
    res.render('index',
               { title :   'Home',
                 channels: channels,
                 channel:  currentChannel })
  }
);

app.get('/channel/:id',
  function (req, res)
  {
    currentChannel = parseInt(req.params.id);
    console.log("Select channel " + req.params.id + " (" + channels[req.params.id].name + ")");
    exec("mpc play " + (currentChannel + 1));
    res.render('index',
               { title: 'Home',
                 channels: channels,
                 channel: req.params.id })
  }
);

// get current playlist, todo: servant streams not updated later...
exec("mpc playlist",
  function(error,stdout,stderr) {
    stations = stdout.split("\n");
    for( var i = 0; i < stations.length - 1; i++ )
    {
      channels[i] = { name: stations[i] };
    }
    console.log("Stations: " + stations);
    console.log("Channels: " + channels);
  }
);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

